package in.nareshit.com.service;

import java.util.List;

import in.nareshit.com.entity.CategoryType;

public interface ICategoryTypeService {
	Long saveCategoryType(CategoryType categorytype);
	void updateCategoryType(CategoryType categorytype);
	void deleteCategoryType(Long id);
	CategoryType getOneCategoryType(Long id);
	List<CategoryType> getAllCategoryTypes();
}

