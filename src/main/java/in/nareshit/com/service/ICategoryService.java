package in.nareshit.com.service;

import java.util.List;

import in.nareshit.com.entity.Category;

public interface ICategoryService {
	Long saveCategory(Category category);
	void updateCategory(Category category);
	void deleteCategory(Long id);
	Category getOneCategory(Long id);
	List<Category> getAllCategorys();


}
