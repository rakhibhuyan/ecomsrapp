package in.nareshit.com.controller;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import in.nareshit.com.entity.Category;
import in.nareshit.com.exception.CategoryNotFoundException;
import in.nareshit.com.service.ICategoryService;

@Controller
@RequestMapping("/category")
public class CategoryController {
	@Autowired
	private ICategoryService service;

	@GetMapping("/register")
	public String registerCategory(Model model) {
		model.addAttribute("category", new Category());
		return "CategoryRegister";
	}

	@PostMapping("/save")
	public String saveCategory(@ModelAttribute Category category, Model model) {
		java.lang.Long id = service.saveCategory(category);
		model.addAttribute("message", "Category created with Id:" + id);
		model.addAttribute("category", new Category());
		return "CategoryRegister";
	}
	@GetMapping("/all")
	public String getAllCategorys(Model model, @RequestParam(value = "message", required = false) String message) {
		List<Category> list = service.getAllCategorys();
		model.addAttribute("list", list);
		model.addAttribute("message", message);
		return "CategoryData";
	}

	@GetMapping("/delete")
	public String deleteCategory(@RequestParam Long id, RedirectAttributes attributes) {
		try {
			service.deleteCategory(id);
			attributes.addAttribute("message", "Category deleted with Id:" + id);
		} catch (CategoryNotFoundException e) {
			e.printStackTrace();
			attributes.addAttribute("message", e.getMessage());
		}
		return "redirect:all";
	}
	@GetMapping("/edit")
	public String editCategory(@RequestParam Long id, Model model, RedirectAttributes attributes) {
		String page = null;
		try {
			Category ob = service.getOneCategory(id);
			model.addAttribute("category", ob);
			page = "CategoryEdit";
		} catch (CategoryNotFoundException e) {
			e.printStackTrace();
			attributes.addAttribute("message", e.getMessage());
			page = "redirect:all";
		}
		return page;
	}

	@PostMapping("/update")
	public String updateCategory(@ModelAttribute Category category, RedirectAttributes attributes) {
		service.updateCategory(category);
		attributes.addAttribute("message", "Category updated");
		return "redirect:all";
	}
}
