package in.nareshit.com.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import in.nareshit.com.entity.CategoryType;


public interface CategoryTypeRepository extends JpaRepository<CategoryType, Long> {

}
