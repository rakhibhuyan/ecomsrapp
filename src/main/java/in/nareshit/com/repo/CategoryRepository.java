package in.nareshit.com.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import in.nareshit.com.entity.Category;

public interface CategoryRepository extends JpaRepository<Category, Long> {

}
